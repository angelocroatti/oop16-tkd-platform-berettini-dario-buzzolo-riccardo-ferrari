package view;

import controller.FormController;

public interface FormStartedPanel {

	public void addObserverForm(FormController controller);
}
