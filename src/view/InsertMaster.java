package view;

import controller.ExamController;

public interface InsertMaster {

	public void addObserver(ExamController controller);
}
