package view;

import controller.ExamController;

public interface InsertAthlete {
	
	public void addObserver(ExamController controller);
}
