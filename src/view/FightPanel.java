package view;

import controller.FightController;

public interface FightPanel {
	
	public void addObserver(FightController controller);
}
